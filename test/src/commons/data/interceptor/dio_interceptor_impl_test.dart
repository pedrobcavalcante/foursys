import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:foursys_album/src/commons/data/interceptor/dio_interceptor.dart';
import 'package:foursys_album/src/commons/data/interceptor/dio_interceptor_impl.dart';
import 'package:mocktail/mocktail.dart';

class DioMock extends Mock implements Dio {}

void main() {
  late Dio dioMock;
  late HTTPRequest dioInterceptor;
  setUp(() {
    dioMock = DioMock();
    dioInterceptor = DioInterceptor(dioMock);
    when(() => dioMock.options).thenReturn(
        BaseOptions(baseUrl: 'http://test.com', connectTimeout: 1000));
  });
  group('Dio Interceptor teste', () {
    test("Get: Should return a Dio instance", () {
      when(() => dioMock.get(any())).thenAnswer(
        (_) => Future.value(
          Response(
            data: {'test': 'test'},
            requestOptions: RequestOptions(method: 'GET', path: '/test'),
          ),
        ),
      );
      expect(dioInterceptor.get('/test'), isA<Future<Response>>());
    });
    test("Post: Should return a Dio instance", () {
      when(() => dioMock.post(any())).thenAnswer(
        (_) => Future.value(
          Response(
            data: {'test': 'test'},
            requestOptions: RequestOptions(method: 'POST', path: '/test'),
          ),
        ),
      );
      expect(dioInterceptor.post('/test'), isA<Future<Response>>());
    });
  });
}
