import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:foursys_album/src/album/data/datasource/album_datasource.dart';
import 'package:foursys_album/src/album/data/model/album_model.dart';
import 'package:foursys_album/src/album/infra/remote/album_datasource_impl.dart';
import 'package:foursys_album/src/commons/data/interceptor/dio_interceptor.dart';

class HTTPRequestMock extends Mock implements HTTPRequest {}

void main() {
  late HTTPRequest httpRequest;
  late AlbumDataSource albumsDataSource;
  setUpAll(() {
    httpRequest = HTTPRequestMock();
    albumsDataSource = AlbumDataSourceImpl(httpRequest);
  });
  group('albums datasource ...', () {
    test("getAll: Should return a list of albums", () async {
      when(() => httpRequest.get(any())).thenAnswer(
        (_) => Future.value(
          Response(
            data: [
              {
                "id": 1,
                "title": "teste",
                "albumId": 1,
                "thumbnailUrl": "teste",
                "url": "teste",
              },
              {
                "id": 1,
                "title": "teste",
                "albumId": 1,
                "thumbnailUrl": "teste",
                "url": "teste",
              },
            ],
            requestOptions: RequestOptions(method: 'GET', path: '/photos/1'),
          ),
        ),
      );
      expect(await albumsDataSource.getAll(), isA<List<AlbumModel>>());
      verify(() => httpRequest.get(any())).called(1);
    });

    test("getById: Should return a album", () async {
      when(() => httpRequest.get(any())).thenAnswer(
        (_) => Future.value(
          Response(
            data: {
              "id": 1,
              "title": "teste",
              "albumId": 1,
              "thumbnailUrl": "teste",
              "url": "teste",
            },
            requestOptions: RequestOptions(method: 'GET', path: '/photos/1'),
          ),
        ),
      );
      expect(await albumsDataSource.getById(1), isA<AlbumModel>());
    });
  });
}
