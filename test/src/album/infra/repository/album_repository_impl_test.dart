import 'package:flutter_test/flutter_test.dart';
import 'package:foursys_album/src/album/data/datasource/album_datasource.dart';
import 'package:foursys_album/src/album/data/entity/album_entity.dart';
import 'package:foursys_album/src/album/data/model/album_model.dart';
import 'package:foursys_album/src/album/data/repository/album_repository.dart';
import 'package:foursys_album/src/album/infra/repository/album_repository_impl.dart';
import 'package:mocktail/mocktail.dart';

class AlbumDatasourceMock extends Mock implements AlbumDataSource {}

void main() {
  group("albumRepository test", () {
    late AlbumDataSource datasource;
    late AlbumRepository albumRepository;
    setUpAll(() {
      datasource = AlbumDatasourceMock();
      albumRepository = AlbumRepositoryImpl(datasource: datasource);
    });
    test("getAll: Should return a list of AlbumEntity", () {
      when(() => datasource.getAll()).thenAnswer((_) => Future.value([
            AlbumModel(
              id: 1,
              title: "teste",
              albumId: 1,
              thumbnailUrl: "teste",
              url: "teste",
            ),
            AlbumModel(
              id: 2,
              title: "teste2",
              albumId: 2,
              thumbnailUrl: "teste",
              url: "teste",
            ),
          ]));
      expect(albumRepository.getAll(), isA<Future<List<AlbumEntity>>>());
      verify(() => datasource.getAll()).called(1);
    });
    test("getById: Should return a AlbumEntity", () {
      when(() => datasource.getById(1)).thenAnswer((_) => Future.value(
          AlbumModel(
              id: 1,
              title: "teste",
              albumId: 1,
              thumbnailUrl: "teste",
              url: "teste")));
      expect(albumRepository.getById(1), isA<Future<AlbumEntity>>());
      verify(() => datasource.getById(1)).called(1);
    });
  });
}
