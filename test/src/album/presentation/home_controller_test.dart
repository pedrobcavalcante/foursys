import 'package:flutter_test/flutter_test.dart';
import 'package:foursys_album/src/album/data/entity/album_entity.dart';
import 'package:foursys_album/src/album/data/repository/album_repository.dart';
import 'package:foursys_album/src/album/presentation/home/home_controller.dart';
import 'package:foursys_album/src/album/presentation/home/home_storage.dart';
import 'package:foursys_album/src/commons/domain/enum/page_status_enum.dart';
import 'package:mocktail/mocktail.dart';

class AlbumRepositoryMock extends Mock implements AlbumRepository {}

void main() {
  group('HomeController test', () {
    late AlbumRepository albumRepository;
    late HomeStorage store;
    late HomeController albumHomeController;
    setUp(() {
      albumRepository = AlbumRepositoryMock();
      store = HomeStorage();
      albumHomeController =
          HomeController(albumRepository: albumRepository, store: store);
    });
    test(
        'Should get new albuns, update album and change page status to loaded.',
        () async {
      final res = AlbumEntity(
          id: 1,
          title: 'Album 1',
          albumId: 1,
          thumbnailUrl: "teste",
          url: "teste");
      when(() => albumRepository.getAll(
              limit: any(named: "limit"), page: any(named: "page")))
          .thenAnswer((_) async => Future.value([res]));
      expect(store.pageStatus, PageStatusEnum.INITIALIZED);

      await albumHomeController.albums();
      expect(store.pageStatus, PageStatusEnum.LOADED);
      expect(store.albums.length, 1);
      expect(store.albums.first, res);
    });
    test('Should get new albuns, return a error.', () async {
      when(() => albumRepository.getAll(
          limit: any(named: "limit"),
          page: any(named: "page"))).thenThrow(Exception());
      expect(store.pageStatus, PageStatusEnum.INITIALIZED);

      await albumHomeController.albums();
      expect(store.pageStatus, PageStatusEnum.ERROR);
      expect(store.albums.length, 0);
    });
    test(
        'Should get new albuns, update album and change page status to loaded.',
        () async {
      final res = AlbumEntity(
          id: 1,
          title: 'Album 1',
          albumId: 1,
          thumbnailUrl: "teste",
          url: "teste");
      when(() => albumRepository.getAll(
              limit: any(named: "limit"), page: any(named: "page")))
          .thenAnswer((_) async => Future.value([res]));
      expect(store.pageStatus, PageStatusEnum.INITIALIZED);

      await albumHomeController.nextPage();
      expect(store.pageStatus, PageStatusEnum.INITIALIZED);
      expect(store.loadingMore, PageStatusEnum.LOADED);
      expect(store.albums.length, 1);
    });
    test('Should update list of albums but return a error.', () async {
      when(() => albumRepository.getAll(
          limit: any(named: "limit"),
          page: any(named: "page"))).thenThrow(Exception());
      expect(store.pageStatus, PageStatusEnum.INITIALIZED);

      await albumHomeController.nextPage();
      expect(store.loadingMore, PageStatusEnum.ERROR);
      expect(store.albums.length, 0);
    });
  });
}
