import 'package:flutter_test/flutter_test.dart';
import 'package:foursys_album/src/album/data/entity/album_entity.dart';
import 'package:foursys_album/src/album/data/repository/album_repository.dart';
import 'package:foursys_album/src/album/presentation/album/album_controller.dart';
import 'package:foursys_album/src/album/presentation/album/home_storage.dart';
import 'package:foursys_album/src/commons/domain/enum/page_status_enum.dart';
import 'package:mocktail/mocktail.dart';

class AlbumRepositoryMock extends Mock implements AlbumRepository {}

void main() {
  group('AlbumController test', () {
    late AlbumRepository albumRepository;
    late AlbumStorage store;
    late AlbumController albumController;
    setUp(() {
      albumRepository = AlbumRepositoryMock();
      store = AlbumStorage();
      albumController =
          AlbumController(albumRepository: albumRepository, store: store);
    });
    test(
        'Should get new albuns, update album and change page status to loaded.',
        () async {
      final res = AlbumEntity(
          id: 1,
          title: 'Album 1',
          albumId: 1,
          thumbnailUrl: "teste",
          url: "teste");
      when(() => albumRepository.getById(any()))
          .thenAnswer((_) async => Future.value(res));
      expect(store.pageStatus, PageStatusEnum.INITIALIZED);

      await albumController.getAlbum(1);
      expect(store.pageStatus, PageStatusEnum.LOADED);
    });
    test('Should get new albuns, return a error.', () async {
      when(() => albumRepository.getAll(
          limit: any(named: "limit"),
          page: any(named: "page"))).thenThrow(Exception());
      expect(store.pageStatus, PageStatusEnum.INITIALIZED);

      await albumController.getAlbum(1);
      expect(store.pageStatus, PageStatusEnum.ERROR);
    });
  });
}
