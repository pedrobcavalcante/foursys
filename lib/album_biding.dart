import 'package:dio/dio.dart';
import 'package:foursys_album/src/album/infra/remote/album_datasource_impl.dart';
import 'package:foursys_album/src/album/presentation/album/album_controller.dart';
import 'package:foursys_album/src/album/presentation/album/home_storage.dart';
import 'package:foursys_album/src/album/presentation/home/home_storage.dart';
import 'package:foursys_album/src/commons/data/interceptor/dio_interceptor_impl.dart';
import 'package:get/get.dart';

import 'src/commons/data/interceptor/dio_interceptor.dart';
import 'src/album/data/datasource/album_datasource.dart';
import 'src/album/data/repository/album_repository.dart';
import 'src/album/infra/repository/album_repository_impl.dart';
import 'src/album/presentation/home/home_controller.dart';

class AlbumBiding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(() => HomeController(
        albumRepository: Get.find(), store: Get.find<HomeStorage>()));
    Get.lazyPut<AlbumController>(() => AlbumController(
        albumRepository: Get.find(), store: Get.find<AlbumStorage>()));
    Get.lazyPut<AlbumRepository>(
        () => AlbumRepositoryImpl(datasource: Get.find()));
    Get.lazyPut<AlbumDataSource>(() => AlbumDataSourceImpl(Get.find()));
    Get.lazyPut<HTTPRequest>(() => DioInterceptor(Dio()));
    Get.lazyPut(() => HomeStorage());
    Get.lazyPut(() => AlbumStorage());
  }
}
