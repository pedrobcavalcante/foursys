import 'package:flutter/material.dart';
import 'package:foursys_album/routes.dart';
import 'package:foursys_album/src/album/presentation/home/home_page.dart';
import 'package:get/get.dart';

import 'album_biding.dart';

void main() {
  runApp(const AppPage());
}

class AppPage extends StatelessWidget {
  const AppPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'My App Flutter',
      theme: ThemeData(),
      home: const HomePage(),
      initialBinding: AlbumBiding(),
      getPages: routes,
      // unknownRoute: GetPage(name: '/notfound', page: () => UnknownRoutePage()),
    );
  }
}
