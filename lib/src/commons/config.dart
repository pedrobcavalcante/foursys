class Config {
  static String get baseUrl => "https://jsonplaceholder.typicode.com/";
  static int get connectTimeout => 10000;
}
