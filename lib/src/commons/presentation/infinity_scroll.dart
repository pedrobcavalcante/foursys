import 'package:flutter/material.dart';

class InfinityScrollList<T> extends StatelessWidget {
  final List<T> items;
  final Future<void> Function()? loadingMore;
  final Widget Function(List<T> listItem, int index) itemBuilder;

  InfinityScrollList(
      {Key? key,
      required this.items,
      this.loadingMore,
      required this.itemBuilder})
      : super(key: key);

  final _scrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    _scrollController.addListener(() {
      if (loadingMore != null &&
          _scrollController.position.pixels ==
              _scrollController.position.maxScrollExtent) {
        loadingMore!();
      }
    });

    return ListView.builder(
      controller: _scrollController,
      itemCount: items.length,
      itemBuilder: (BuildContext context, int index) {
        return itemBuilder(items, index);
      },
    );
  }
}
