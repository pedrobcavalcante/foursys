import 'package:dio/dio.dart';

abstract class HTTPRequest {
  Future<Response<T>> get<T>(String path);
  Future<Response<T>> post<T>(String path, {data, options});
}
