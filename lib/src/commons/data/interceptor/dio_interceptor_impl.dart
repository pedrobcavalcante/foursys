import 'package:dio/dio.dart';

import '../../config.dart';
import 'dio_interceptor.dart';

class DioInterceptor implements HTTPRequest {
  final Dio _dio;

  DioInterceptor(this._dio) {
    _dio.options = BaseOptions(
      baseUrl: Config.baseUrl,
      connectTimeout: Config.connectTimeout,
    );
  }

  @override
  Future<Response<T>> get<T>(String path) async {
    return _dio.get<T>(path);
  }

  @override
  Future<Response<T>> post<T>(String path, {data, options}) async {
    return _dio.post<T>(path, data: data, options: options);
  }
}
