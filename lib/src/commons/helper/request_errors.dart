abstract class StatusResponse<T> {
  late final int _statusCode;
  late final String? _message;
  late final T? _result;
  StatusResponse({
    required statusCode,
    message,
    result,
  }) {
    _statusCode = statusCode;
    _message = message;
    _result = result;
  }

  get statusCode => _statusCode;
  get message => _message;
  get result => _result;
}

abstract class SucessResponse<T> extends StatusResponse<T> {
  SucessResponse({
    int statusCode = 200,
    String? message,
    T? result,
  }) : super(statusCode: statusCode, message: message, result: result);
}

class OkResponse<T> extends SucessResponse<T> {
  OkResponse({T? result}) : super(result: result);
}

class CreateResponse<T> extends SucessResponse<T> {
  CreateResponse({String? message, T? result})
      : super(message: message, result: result);
  @override
  int get statusCode => _statusCode + 1;
}

class AcceptedResponse<T> extends SucessResponse<T> {
  AcceptedResponse({String? message, T? result});

  @override
  int get statusCode => _statusCode + 2;
}

class NoContentResponse<T> extends SucessResponse<T> {
  NoContentResponse({String? message, T? result});

  @override
  int get statusCode => _statusCode + 4;
}

abstract class ErrorResponse<T> extends StatusResponse {
  ErrorResponse({
    String? message,
    T? result,
    int statusCode = 400,
  }) : super(
          statusCode: statusCode,
          message: message,
          result: result,
        );
}

class BadRequest<T> extends ErrorResponse<T> {
  BadRequest({String? message, T? result});
}

class Unauthorized<T> extends ErrorResponse<T> {
  Unauthorized({String? message, T? result});

  @override
  int get statusCode => _statusCode + 1;
}

class PaymentRequiredResponse<T> extends ErrorResponse<T> {
  PaymentRequiredResponse({String? message, T? result});

  @override
  int get statusCode => _statusCode + 2;
}

class ForbiddenResponse<T> extends ErrorResponse<T> {
  ForbiddenResponse({String? message, T? result});

  @override
  int get statusCode => _statusCode + 3;
}

class NotFound<T> extends ErrorResponse<T> {
  NotFound({String? message, T? result});

  @override
  int get statusCode => _statusCode + 4;
}

abstract class ServerErrorResponse<T> extends StatusResponse {
  ServerErrorResponse({
    String? message,
    T? result,
    int statusCode = 500,
  }) : super(
          statusCode: statusCode,
          message: message,
          result: result,
        );
}

class InternalServerError<T> extends ServerErrorResponse<T> {
  InternalServerError({String? message, T? result});

  @override
  int get statusCode => _statusCode;
}

class NotImplementedResponse<T> extends ServerErrorResponse<T> {
  NotImplementedResponse({String? message, T? result});

  @override
  int get statusCode => _statusCode + 1;
}

class ServiceUnavailable<T> extends ServerErrorResponse<T> {
  ServiceUnavailable({String? message, T? result});

  @override
  int get statusCode => _statusCode + 3;
}
