import 'package:get/get_state_manager/get_state_manager.dart';

import '../../../commons/domain/enum/page_status_enum.dart';
import '../../data/repository/album_repository.dart';
import 'home_storage.dart';

class AlbumController extends GetxController {
  final AlbumRepository albumRepository;
  final AlbumStorage store;
  AlbumController({required this.albumRepository, required this.store});

  Future<void> getAlbum(int id) async {
    try {
      store.pageStatus = PageStatusEnum.LOADING;
      store.setAlbum = await albumRepository.getById(id);
      store.pageStatus = PageStatusEnum.LOADED;
    } catch (e) {
      store.pageStatus = PageStatusEnum.ERROR;
    }
  }
}
