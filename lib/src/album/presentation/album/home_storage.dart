// ignore_for_file: invalid_use_of_protected_member

import 'package:get/get.dart';

import '../../../commons/domain/enum/page_status_enum.dart';
import '../../data/entity/album_entity.dart';

class AlbumStorage {
  final _pageStatus = PageStatusEnum.INITIALIZED.obs;
  PageStatusEnum get pageStatus => _pageStatus.value;
  set pageStatus(PageStatusEnum value) => _pageStatus.value = value;

  final _album = Rx<AlbumEntity?>(null);
  AlbumEntity? get album => _album.value;
  set setAlbum(AlbumEntity value) => _album.value = value;
}
