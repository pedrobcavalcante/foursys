import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../commons/domain/enum/page_status_enum.dart';
import '../../../commons/presentation/base_page.dart';
import '../../data/entity/album_entity.dart';
import 'album_controller.dart';

class AlbumPage extends GetView<AlbumController> {
  static const routeName = '/album';
  const AlbumPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final id = Get.parameters['id'];
    return BasePage(
      title: "Album $id",
      body: Obx(() => _bodyGenerator(context, id!)),
    );
  }

  Widget _bodyGenerator(BuildContext context, String id) {
    switch (controller.store.pageStatus) {
      case PageStatusEnum.INITIALIZED:
        controller.getAlbum(int.parse(id));
        return const Center(child: CircularProgressIndicator());
      case PageStatusEnum.LOADING:
        return const Center(child: CircularProgressIndicator());
      case PageStatusEnum.LOADED:
        return Row(
          children: [
            const Spacer(),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const SizedBox(height: 20),
                _imageGenerator(controller.store.album?.thumbnailUrl),
                const SizedBox(height: 20),
                _description(controller.store.album),
              ],
            ),
            const Spacer(),
          ],
        );
      case PageStatusEnum.ERROR:
        return const Center(child: Text("Erro na Obtenção dos dados."));
    }
  }

  Widget _description(AlbumEntity? album) {
    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 400),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          if (controller.store.album?.title != null)
            _text("Titulo: ", controller.store.album!.title),
          const SizedBox(height: 10),
          if (controller.store.album?.id != null)
            _text("Id: ", controller.store.album!.id.toString()),
          const SizedBox(height: 10),
          if (controller.store.album?.albumId != null)
            _text("Id do Album: ", controller.store.album!.albumId.toString()),
          const SizedBox(height: 10),
        ],
      ),
    );
  }

  Widget _text(String left, String right) {
    const style = TextStyle(
        fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black);
    const style2 = TextStyle(fontSize: 18, color: Colors.grey);
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(left, style: style),
        const SizedBox(width: 10),
        Flexible(
          child: Text(
            right,
            style: style2,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ),
        )
      ],
    );
  }

  Widget _imageGenerator(String? url, {bool retry = true}) {
    return Image.network(
      url ?? "",
      height: Get.height / 1.7,
      fit: BoxFit.cover,
      errorBuilder: (context, error, stackTrace) {
        if (retry) {
          return _imageGenerator(
              "https://picsum.photos/600/?random&t=${Random().nextDouble()}",
              retry: false);
        }
        return const Icon(Icons.error);
      },
      loadingBuilder: (context, child, progress) {
        return progress == null
            ? child
            : const Center(child: CircularProgressIndicator());
      },
    );
  }
}
