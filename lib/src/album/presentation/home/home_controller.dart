import 'package:foursys_album/src/album/data/repository/album_repository.dart';
import 'package:foursys_album/src/commons/domain/enum/page_status_enum.dart';
import 'package:get/get.dart';

import 'home_storage.dart';

class HomeController extends GetxController {
  final AlbumRepository albumRepository;
  final HomeStorage store;
  HomeController({required this.albumRepository, required this.store});

  Future<void> albums() async {
    try {
      store.pageStatus = PageStatusEnum.LOADING;
      final newList = await albumRepository.getAll(limit: 20, page: store.page);
      store.albumsAdd = newList;
      store.pageStatus = PageStatusEnum.LOADED;
    } catch (e) {
      store.pageStatus = PageStatusEnum.ERROR;
    }
  }

  Future<void> nextPage() async {
    if (store.loadingMore == PageStatusEnum.LOADING &&
        (store.pageStatus == PageStatusEnum.LOADED ||
            store.pageStatus == PageStatusEnum.INITIALIZED)) return;

    try {
      store.loadingMore = PageStatusEnum.LOADING;
      store.page = store.page + 1;
      final newList = await albumRepository.getAll(limit: 20, page: store.page);
      store.albumsAdd = newList;
      store.loadingMore = PageStatusEnum.LOADED;
    } catch (e) {
      store.loadingMore = PageStatusEnum.ERROR;
    }
  }
}
