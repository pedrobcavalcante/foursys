import 'dart:math';

import 'package:flutter/material.dart';
import 'package:foursys_album/src/commons/domain/enum/page_status_enum.dart';
import 'package:get/get.dart';
import '../../../commons/presentation/base_page.dart';
import '../../../commons/presentation/infinity_scroll.dart';
import '../../data/entity/album_entity.dart';
import '../album/album_page.dart';
import 'home_controller.dart';

class HomePage extends GetView<HomeController> {
  static const routeName = '/home';
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BasePage(
      title: "Albuns",
      body: Obx(() => _bodyGenerator(context)),
    );
  }

  Widget _bodyGenerator(BuildContext context) {
    switch (controller.store.pageStatus) {
      case PageStatusEnum.INITIALIZED:
        controller.albums();
        return const Center(child: CircularProgressIndicator());
      case PageStatusEnum.LOADING:
        return const Center(child: CircularProgressIndicator());
      case PageStatusEnum.LOADED:
        return InfinityScrollList<AlbumEntity>(
          items: controller.store.albums,
          loadingMore: controller.nextPage,
          itemBuilder: itemBuilder,
        );
      case PageStatusEnum.ERROR:
        return const Center(child: Text("Erro na Obtenção dos dados."));
    }
  }

  Widget itemBuilder(List<AlbumEntity> listItem, int index) {
    return ListTile(
      title: Text(listItem[index].title),
      subtitle: Text(
          "User ID: ${listItem[index].albumId} - Album ID: ${listItem[index].id}"),
      onTap: () {
        Get.toNamed(AlbumPage.routeName + "/${listItem[index].id}");
      },
      leading: _imageGenerator(listItem[index].thumbnailUrl),
    );
  }

  Widget _imageGenerator(String url, {bool retry = true}) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(100),
      child: Image.network(
        url,
        height: 50,
        width: 50,
        fit: BoxFit.cover,
        errorBuilder: (context, error, stackTrace) {
          if (retry) {
            return _imageGenerator(
                "https://picsum.photos/200/300/?random&t=${Random().nextDouble()}",
                retry: false);
          }
          return const Icon(Icons.error);
        },
        loadingBuilder: (context, child, progress) {
          return progress == null
              ? child
              : const Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}
