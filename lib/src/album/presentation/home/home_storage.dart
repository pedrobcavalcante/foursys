// ignore_for_file: invalid_use_of_protected_member

import 'package:get/get.dart';

import '../../../commons/domain/enum/page_status_enum.dart';
import '../../data/entity/album_entity.dart';

class HomeStorage {
  final _pageStatus = PageStatusEnum.INITIALIZED.obs;
  PageStatusEnum get pageStatus => _pageStatus.value;
  set pageStatus(PageStatusEnum value) => _pageStatus.value = value;

  final _loadingMore = PageStatusEnum.INITIALIZED.obs;
  PageStatusEnum get loadingMore => _loadingMore.value;
  set loadingMore(PageStatusEnum value) => _loadingMore.value = value;

  final _albums = <AlbumEntity>[].obs;
  List<AlbumEntity> get albums => _albums.value;
  set albums(List<AlbumEntity> value) => _albums.value = value;
  set albumsAdd(List<AlbumEntity> value) =>
      _albums.value = _albums.value + value;

  final _page = 0.obs;
  int get page => _page.value;
  set page(int value) => _page.value = value;
}
