import 'package:foursys_album/src/album/data/datasource/album_datasource.dart';

import '../../data/entity/album_entity.dart';
import '../../data/mapper/album_mapper.dart';
import '../../data/repository/album_repository.dart';

class AlbumRepositoryImpl implements AlbumRepository {
  final AlbumDataSource datasource;
  AlbumRepositoryImpl({required this.datasource});

  @override
  Future<List<AlbumEntity>> getAll({int? page, int? limit}) async {
    final response = await datasource.getAll(limit: limit, page: page);
    return response.map((e) => AlbumMapper.toEntity(e)).toList();
  }

  @override
  Future<AlbumEntity> getById(int id) async {
    final response = await datasource.getById(id);
    return AlbumMapper.toEntity(response);
  }
}
