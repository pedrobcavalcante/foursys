import 'package:foursys_album/src/album/data/mapper/album_mapper.dart';

import '../../../commons/data/interceptor/dio_interceptor.dart';
import '../../data/datasource/album_datasource.dart';
import '../../data/model/album_model.dart';

class AlbumDataSourceImpl implements AlbumDataSource {
  final _path = "/photos";
  AlbumDataSourceImpl(this.client);

  final HTTPRequest client;

  @override
  Future<List<AlbumModel>> getAll({int? page, int? limit}) async {
    String path = _path;
    path += limit != null || page != null ? '?' : '';
    path += limit != null ? '_limit=$limit' : '';
    path += limit != null && page != null ? '&' : '';
    path += page != null ? '_page=$page' : '';
    final response = await client.get(path);
    return (response.data as List)
        .map((e) => AlbumMapper.fromJson(e as Map<String, dynamic>))
        .toList();
  }

  @override
  Future<AlbumModel> getById(int id) async {
    final response = await client.get('$_path/$id');
    return AlbumMapper.fromJson(response.data as Map<String, dynamic>);
  }
}
