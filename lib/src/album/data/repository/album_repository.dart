import '../entity/album_entity.dart';

abstract class AlbumRepository {
  Future<List<AlbumEntity>> getAll({int? page, int? limit});
  Future<AlbumEntity> getById(int id);
}
