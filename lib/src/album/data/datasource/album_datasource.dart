import '../model/album_model.dart';

abstract class AlbumDataSource {
  Future<List<AlbumModel>> getAll({int? page, int? limit});
  Future<AlbumModel> getById(int id);
}
