import '../entity/album_entity.dart';
import '../model/album_model.dart';

class AlbumMapper {
  static AlbumModel fromJson(Map<String, dynamic> json) {
    final albumId = json['albumId'] ?? "";
    final id = json['id'];
    final title = json['title'];
    final url = json['url'];
    final thumbnailUrl = json['thumbnailUrl'];
    return AlbumModel(
        albumId: albumId,
        id: id,
        title: title,
        thumbnailUrl: thumbnailUrl,
        url: url);
  }

  static Map<String, dynamic> toJson(AlbumModel model) {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['albumId'] = model.albumId;
    data['id'] = model.id;
    data['title'] = model.title;
    data['url'] = model.url;
    data['thumbnailUrl'] = model.thumbnailUrl;
    return data;
  }

  static AlbumEntity toEntity(AlbumModel model) {
    return AlbumEntity(
      albumId: model.albumId,
      id: model.id,
      title: model.title,
      thumbnailUrl: model.thumbnailUrl,
      url: model.url,
    );
  }
}
