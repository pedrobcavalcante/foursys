import 'package:foursys_album/src/album/presentation/album/album_page.dart';
import 'package:get/get.dart';

import 'src/album/presentation/home/home_page.dart';

final routes = [
  GetPage(
    name: '/',
    page: () => const HomePage(),
  ),
  GetPage(
    name: HomePage.routeName,
    page: () => const HomePage(),
  ),
  GetPage(
    name: AlbumPage.routeName + '/:id',
    page: () => const AlbumPage(),
  ),
];
